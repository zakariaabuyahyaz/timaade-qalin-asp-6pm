﻿using System;
using System.Collections.Generic;

namespace GtxApp.Models;

public partial class Book
{
    public int Id { get; set; }

    public string? Title { get; set; }

    public string? Author { get; set; }

    public string? Publisher { get; set; }

    public DateTime? Publisheddate { get; set; }

    public string? Isbn { get; set; }

    public int? Availablecopies { get; set; }

    public string? Ruser { get; set; }

    public DateTime? Registereddate { get; set; }

    public virtual ICollection<Borrowedbook> Borrowedbooks { get; set; } = new List<Borrowedbook>();
}
