﻿using GtxApp.Data;
using GtxApp.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;

namespace GtxApp.Controllers
{
    public class customersController : Controller
    {
        private readonly qalinContext _context;

        public customersController()
        {
            _context = new qalinContext();
        }
        public IActionResult Index()
        {
            if (TempData["msg"] != null)
            {
                ViewData["msg"] = TempData["msg"].ToString();
            }

            if (TempData["error"] != null)
            {
                ViewData["error"] = TempData["error"].ToString();
            }

            //find customers from the db
            var _list = _context.Customers.OrderByDescending(k=>k.Id).ToList();

            return View(_list);
        }

        [HttpGet]
        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public IActionResult Create([Bind("Fullname, Mobile, Address, Email")] Customer model)
        {
            try
            {
                model.Registereddate = DateTime.Now;
                model.Ruser = "Abuyahya";
                _context.Customers.Add(model);
                _context.SaveChanges();
                TempData["msg"] = "New Customer Created Successfully";

            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
            }
            return RedirectToAction("Index");

        }

        [HttpGet]
        public IActionResult Edit(int id)
        {
            //find customer
            Customer _found = _context.Customers.Find(id);
            return View(_found);

        }

        [HttpPost]
        public IActionResult Edit([Bind("Id, Fullname, Mobile, Address, Email")] Customer model)
        {
            try
            {
                Customer _found = _context.Customers.Find(model.Id);
                _found.Fullname = model.Fullname;
                _found.Address = model.Address;
                _found.Email = model.Email;
                _found.Mobile = model.Mobile;
                _context.Customers.Update(_found);
                _context.SaveChanges();
                TempData["msg"] = "Customer Data Edited Successfully";
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
            }
            return RedirectToAction("Index");


        }

        public IActionResult Delete(int id)
        {
            Customer _found = _context.Customers.Find(id);
            return View(_found);
        }

        [HttpPost]
        public IActionResult Delete([Bind("Id")] Customer model)
        {
            try
            {
                Customer _found = _context.Customers.Find(model.Id);
                _context.Customers.Remove(_found);
                _context.SaveChanges();
                TempData["msg"] = "Customer Deleted Successfully";
            }
            catch (Exception ex)
            {
                TempData["error"] = ex.Message;
            }

            return RedirectToAction("Index");


        }






    }
}
