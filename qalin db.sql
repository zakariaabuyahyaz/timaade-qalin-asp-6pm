Create database Qalin_db
Go

use Qalin_db
go
CREATE TABLE customers (
  id INT PRIMARY KEY identity,
  fullname VARCHAR(50),
  address VARCHAR(50),
  email VARCHAR(100),
  mobile VARCHAR(20),
  ruser varchar(50),
  registereddate smalldatetime
)

CREATE TABLE books (
  id INT PRIMARY KEY identity,
  title VARCHAR(100),
  author VARCHAR(100),
  publisher VARCHAR(100),
  publisheddate DATE,
  isbn VARCHAR(50),
  availablecopies INT,
  ruser varchar(50),
  registereddate smalldatetime
)

CREATE TABLE borrowedbooks (
  id INT PRIMARY KEY identity,
  bookid INT foreign key references books (id),
  customerid INT foreign key references customers (id),
  borroweddate DATE,
  returndate DATE,
  status varchar(100),
  ruser varchar(50),
  registereddate smalldatetime
)


-- Seeding the database
INSERT INTO customers (fullname, address, email, mobile, ruser, registereddate)
VALUES 
('John Doe', '123 Main St', 'johndoe@example.com', '555-1234', 'admin', GETDATE()),
('Jane Doe', '456 Elm St', 'janedoe@example.com', '555-5678', 'admin', GETDATE()),
('Bob Smith', '789 Oak St', 'bobsmith@example.com', '555-9012', 'admin', GETDATE()),
('Alice Johnson', '246 Maple St', 'alicejohnson@example.com', '555-3456', 'admin', GETDATE()),
('David Lee', '369 Pine St', 'davidlee@example.com', '555-7890', 'admin', GETDATE())

INSERT INTO books (title, author, publisher, publisheddate, isbn, availablecopies, ruser, registereddate)
VALUES 
('The Great Gatsby', 'F. Scott Fitzgerald', 'Scribner', '1925-04-10', '978-0743273565', 3, 'admin', GETDATE()),
('To Kill a Mockingbird', 'Harper Lee', 'J. B. Lippincott & Co.', '1960-07-11', '978-0446310789', 5, 'admin', GETDATE()),
('1984', 'George Orwell', 'Secker and Warburg', '1949-06-08', '978-0451524935', 2, 'admin', GETDATE()),
('The Catcher in the Rye', 'J. D. Salinger', 'Little, Brown and Company', '1951-07-16', '978-0316769174', 4, 'admin', GETDATE()),
('Brave New World', 'Aldous Huxley', 'Chatto & Windus', '1932-06-18', '978-0060850524', 1, 'admin', GETDATE())

INSERT INTO borrowedbooks (bookid, customerid, borroweddate, returndate, status, ruser, registereddate)
VALUES 
(1, 1, '2022-04-01', '2022-04-08', 'returned', 'admin', GETDATE()),
(2, 2, '2022-04-02', '2022-04-09', 'returned', 'admin', GETDATE()),
(3, 3, '2022-04-03', '2022-04-10', 'returned', 'admin', GETDATE()),
(4, 4, '2022-04-04', '2022-04-11', 'returned', 'admin', GETDATE()),
(5, 5, '2022-04-05', NULL, 'borrowed', 'admin', GETDATE())


select * from customers
select * from books
select * from borrowedbooks